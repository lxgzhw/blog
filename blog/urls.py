from django.contrib import admin
from django.urls import path, include, re_path
from myblog import views

# 导入静态文件模块
from django.views.static import serve
# 导入配置文件里面的上传配置
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('ueditor/', include('DjangoUeditor.urls')),
    # 添加以下路由,让富文本编辑器可以显示图片
    re_path('^media/(?P<path>.*)$', serve,
            {'document_root': settings.MEDIA_ROOT}),

    # 文章页面路由
    # 网站首页
    path('', views.index, name='index'),
    # 列表页
    path('list-<int:lid>.html', views.list, name='list'),
    path('show-<int:sid>.html', views.show, name='show'),
    path('tag/<tag>', views.tag, name='tags'),
    path('s/', views.search, name='search'),
    path('about/', views.about, name='about'),

]
