# 一款基于Django2.1最新版的Blog
- 功能
    - 文章列表
    - 分页
    - 文章详情
    - 友情链接
    - 热门文章
    - 随机推荐
    - 最新文章
    - 阅读量自增
    - 轮播图
    
## 安装环境
- 查看根目录下的requirements.txt
- pip install -r requirements.txt

## 这是版本1,尚未加入用户登录注册功能,后期维护增加
## 如果您想要学习怎么做这个作品,可以报名跟我一起学Python



## 联系方式
- 微信 zhangdapneg99
- QQ  1156956636
- 抖音 理想国真恵玩
- 博客 http://lxgzhw520.com