from django.shortcuts import render, HttpResponse
from .models import Category, Banner, Article, Tag, Link
# 分页
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.
HOST_ARTICLE = 2  # 推荐文章的值为2


def common():
    # 查询出阅读量最高的10篇文章 最热
    most_views_articles = Article.objects.all().order_by('-views')[0:10]
    # 右侧热门推荐文章
    right_articles = Article.objects.filter(tui__id=2)[:6]
    # 右侧所有标签的实现
    right_tags = Tag.objects.all()[:20]
    allcategory = Category.objects.all()
    return (most_views_articles, right_tags, right_articles, allcategory)


# 首页
def index(request):
    allcategory = Category.objects.all()
    # 查询出四张幻灯片
    banners = Banner.objects.filter(is_active=True)[0:4]
    # 查询出3篇推荐的文章
    tuis = Article.objects.all().order_by('-views')[:3]  # 查询推荐位ID为1的文字 1代表推荐
    # 最新的10篇文章
    latest_articles = Article.objects.all().order_by('-created_time')[0:10]
    # 查询出阅读量最高的10篇文章 最热
    most_views_articles = Article.objects.all().order_by('-views')[0:10]
    # 右侧热门推荐文章
    right_articles = Article.objects.filter(tui__id=2)[:6]
    # 右侧所有标签的实现
    right_tags = Tag.objects.all()[:20]
    # 友情链接
    links = Link.objects.all()[:20]
    context = {
        'allcategory': allcategory,
        'banners': banners,
        'latest_articles': latest_articles,
        'most_views_articles': most_views_articles,
        'right_articles': right_articles,
        'right_tags': right_tags,
        'tuis': tuis,
        'links': links,
    }
    return render(request, 'index.html', context)


# 列表页
def list(request, lid):
    # 查询分类的的所有文章
    list_articles = Article.objects.filter(category_id=lid)
    cname = Category.objects.filter(id=lid).first()
    # 从url中获取当夜页码的记录
    page = request.GET.get('page')
    # 对查询到的数据对象list进行分页,设置超过5条数据就分页
    paginator = Paginator(list_articles, 1)
    try:
        list = paginator.page(page)
    except PageNotAnInteger as e:
        # 如果页码不是整数,就显示第一页
        list = paginator.page(1)
    except EmptyPage:
        # 如果输入的页数不在系统的页码列表中,就显示最后一页
        list = paginator.page(paginator.num_pages)
    most_views_articles, right_tags, right_articles, allcategory = common()
    return render(request, 'list.html', locals())


# 内容页
def show(request, sid):
    article = Article.objects.get(id=sid)
    # 随机推荐
    hot = Article.objects.all().order_by('?')[:10]
    # 上一篇
    previos_blog = Article.objects.filter(created_time__gt=article.created_time,

                                          category=article.category.id).first()
    # 下一篇
    next_blog = Article.objects.filter(created_time__lt=article.created_time,
                                       category=article.category.id).last()
    # 浏览量自增 django2新语法
    article.views = article.views + 1
    article.save()

    most_views_articles, right_tags, right_articles, allcategory = common()
    return render(request, 'show.html', locals())


# 标签页
def tag(request, tag):
    # 通标签查询文章 tags字段 多对多正向查询 双下划线
    list = Article.objects.filter(tags__name=tag)
    most_views_articles, right_tags, right_articles, allcategory = common()
    tname = Tag.objects.get(name=tag)  # 获取当前搜索的标签名
    page = request.GET.get('page')
    # 每页有五条数据
    paginator = Paginator(list, 5)
    try:
        # 默认跳转到请求页
        list = paginator.page(page)
    except PageNotAnInteger as e:
        # 非数字就是第一页
        list = paginator.page(1)
    except EmptyPage:
        # 空的就是最后一页
        list = paginator.page(paginator.num_pages)
    return render(request, 'tags.html', locals())


# 搜索页
def search(request):
    #获取搜索的关键词
    ss=request.GET.get('search')
    # 通标关键词查询文章
    list = Article.objects.filter(title__contains=ss)
    most_views_articles, right_tags, right_articles, allcategory = common()
    page = request.GET.get('page')
    # 每页有五条数据
    paginator = Paginator(list, 5)
    try:
        # 默认跳转到请求页
        list = paginator.page(page)
    except PageNotAnInteger as e:
        # 非数字就是第一页
        list = paginator.page(1)
    except EmptyPage:
        # 空的就是最后一页
        list = paginator.page(paginator.num_pages)
    return render(request, 'search.html', locals())


# 关于我们
def about(request):
    allcategory=Category.objects.all()
    return render(request,'page.html',locals())
