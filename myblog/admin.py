from django.contrib import admin
from .models import Banner, Category, Article, Tag, Tui, Link


# Register your models here.


# 文章管理
@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ['id', 'category', 'title', 'tui', 'user', 'views', 'created_time']
    list_per_page = 50  # 自动分页 每页50条
    ordering = ('-created_time',)  # 根据创建时间逆序 最新的在前
    list_display_links = ['id', 'title']  # 设置哪些字段可以进入编辑页面


# 轮播图管理
@admin.register(Banner)
class BannerAdmin(admin.ModelAdmin):
    list_display = ['id', 'text_info', 'img', 'link_url', 'is_active']


# 分类管理
@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


# 标签管理
@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


# 推荐管理
@admin.register(Tui)
class TuiAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


# 友情链接管理
@admin.register(Link)
class LinkAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'linkurl']
